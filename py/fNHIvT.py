'''
Rohan Choudhury
fNHIvT.py
This will use the code from the fNHI class
to plot fNHI versus time for many different
 instances in time. it will overlay the fNHI
 plots over each other in matplotlib.
'''

import matplotlib # imports necessary classes
matplotlib.use('Agg')
import fNHI as fN
from math import log
import numpy as np
import matplotlib.pyplot as plt

class fNHIvT:
    def __init__(self,expname,times):#takes in experiment name
        self.name = expname
        self.time = times
    def plot(self):#uses fNHI class to get data and plots for various time instances
        plt.figure()
        for i in range(self.time):
            plot = fN.fNHI(self.name,i)
            x,y = plot.createHistogram()
            plt.plot((x),np.log10(y))
            plt.ylabel(r'f(N$_{H\mathrm{I}}$)')#labels axes with LaTeX
            plt.xlabel(r'N$_{H\mathrm{I}}$')
        plt.savefig(self.name + "fNHIvT.png")#saves final figure

if __name__ == '__main__':#main method
    graph = fNHIvT('rg02a',100)
    graph.plot()
    
