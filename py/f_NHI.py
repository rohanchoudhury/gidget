#Rohan Choudhury
#f_NHI.py
# will calculate f(NHI,z) without using bins 
# and by making various approximations to remove spikes 
# from graph of dlogf(NHI)/dlogp. contains a bunch of methods
# to understand more about f(NHI,z)

from readoutput import *
import matplotlib.pyplot as plt
from math import log10

cmperpc = 3.08567758e18
hiperg = 1e24
gpermsun = 1.9891e33

class f_NHI:
    def __init__(self,expName,time):
        self.ti = time
        self.exp = Experiment(expName)
        (self.exp).read()
    def g(self,NHI,NHI_i,dA_i):#auxiliary function, attempts to simulate fPerp
        if (NHI<NHI_i):
            return 0
        else:
            return NHI_i ** 2.0 * dA_i
    def f(self,NHI,NHIs,dAs): # calculates f(NHI) given the i'th radial bin
        f = 0
        for j in range(len(NHIs)):
            f = f + f_NHI.g(self,NHI,NHIs[j],dAs[j])
        print f/NHI**3.0
        return f/NHI**3.0
    def fNHI(self,nh1dat):# creates an array given a set of NHIs
        dadat = f_NHI.getdAs(self)
        fNHIs = []
        
        for j in range(len(nh1dat)):
            fNHIs.append(f_NHI.f(self,nh1dat[j],nh1dat,dadat))
        return nh1dat,fNHIs
    def getdAs(self,model = 0):#returns an array of annuli for the exp 
        return (self.exp).models[model].var['dA'].sensible(self.ti)
    def getNHIs(self,model = 0):#returns an array of NHI for the exp
        dat = (self.exp).models[model].var['col'].sensible(self.ti)
        return dat * gpermsun * cmperpc ** -2 * hiperg

if __name__ == '__main__':
    fN = f_NHI('aa03',201)
    nh1dat = fN.getNHIs()
    print nh1dat
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x,y = fN.fNHI(nh1dat)
    plt.plot(np.log10(x),np.log10(y))
    fN1 = f_NHI('te05',201)
    nh1dat = fN1.getNHIs()
    x,y = fN1.fNHI(nh1dat)
    ax.set_xlim(17.5,22.0)
    plt.plot(np.log10(x),np.log10(y),color = 'green')
    plt.savefig('te05fNHI.png')

    
    
