#Rohan Choudhury
#fNHIParamvT
#Runs the fNHIParam for many time instances.

from readoutput import *
import numpy as np
import matplotlib.pyplot as plt
from fNHIParam import *

class fNHIParamvT:
    def __init__(self,expName,param):
        self.name = expName
        self.param = param
    def plot(self):
        par1 = fNHIParam(self.name,self.param,0) 
        par2 = fNHIParam(self.name,self.param,51)
        par3 = fNHIParam(self.name,self.param,201)
        plt.figure()
        x,y = par1.getChange()
        plt.plot(x,y)
        x2,y2 = par2.getChange()
        plt.plot(x2,y2)
        x3,y3 = par3.getChange()
        plt.plot(x3,y3)
        plt.ylabel(r'$d\log_{10}f(N_{\mathrm{H}\,textsc{i}}/d\log_{10}p$')
        plt.xlabel(r'N$_{H\mathrm{I}}$')
        plt.savefig("tester.png")
if __name__ == '__main__':
    fN = fNHIParamvT('rd02','epsff')
    fN.plot()
