#Rohan Choudhury
#FPerpDistribution.py
#this module will create the fPerp distribution for a simulated galaxy
#from the gidget code.

from readoutput import * 
import numpy as np
from math import log10

cmperpc = 3.08567758e18 # necessary conversion factors and constants
gpermsun = 1.9891e33
binnum = 1000
hiperg = 1e24
extrabins = 10

class FPerp:
    def __init__(self,expName,time):# takes in experiment name and time instance
        self.name = expName
        self.ti = time
        self.edge = 0
    def getCenters(self,expname,modelnum = 0):
        exp = Experiment(expname)
        exp.read()
        nh1dat = exp.models[modelnum].var['col'].sensible(0)
        nh1dat = nh1dat * gpermsun * pow(cmperpc,-2) * hiperg
        binCenters =[0] * (binnum + extrabins-1)
        
        self.edge = (nh1dat[0]-nh1dat[len(nh1dat)-1])/binnum
        for i in range(len(binCenters)):
            binCenters[i] = self.edge/2 + i * self.edge
          
        return binCenters
    def getVals(self,model = 0):
        binCenters = FPerp.getCenters(self,'rg01',modelnum = 0)
        binVals = [0] * (binnum + extrabins-1)
        binVals2 = [0] * (binnum + extrabins-1)

        exp = Experiment(self.name)
        exp.read()
        
        nh1dat = np.array(exp.models[model].var['col'].sensible(self.ti))
        nh1dat = nh1dat * gpermsun * pow(cmperpc,-2) * hiperg
        dadat = np.array(exp.models[model].var['dA'].sensible(self.ti))
        
        for i in range(len(nh1dat)):#sort the data into bins
            index,val = Nearest(binCenters,nh1dat[i])
            binVals[index] = binVals[index] +dadat[i]/val

        return np.log10(binCenters),binVals

    def getEdge(self):#returns edge, needed for client classes
        return self.edge
if __name__ == '__main__':
    fP = FPerp('rg01',0)
    x,y = fP.getVals()
    plt.plot(x,y)
    plt.savefig('test')
