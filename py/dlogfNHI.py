# Rohan Choudhury
# dlogfNHI.py
# This calculates the logarithmic derivative(dlogf(NHI,z)/dlogp)
# for an experiment with a varied parameter with 
# the approximation module.

from readoutput import *
from f_NHI import *
from math import log,log10

class dlogfNHI:
    def __init__(self,expname,expname2,param,time1, time2):
        self.ti1 = time1
        self.ti2 = time2
       # print self.ti2
        self.name = expname2
        global fN1
        fN1 = f_NHI(expname,self.ti1)
        global fN2 
        fN2= f_NHI(expname2,self.ti2)
        dA = True
        for i in range(len(fN1.getdAs())):
            if(fN1.getdAs()[i] != fN2.getdAs()[i]):
                dA = False
        if dA == False:
            print "Sorry, the dA's don't match!"
            exit()
        global exp
        exp = Experiment(expname)
        exp.read()
        global exp2
        exp2 = Experiment(expname2)
        exp2.read()
        self.param = param
    def dlogf(self):
         dlogfdlogps = ([])
         NHIs = []
         params,_,_,_ = exp2.constructQuantity(self.param)
         fids,_,_,_ = exp.constructQuantity(self.param)
         fid = fids[0]
             
         NHI1 = fN1.getNHIs(model = 0)
         NHI2 = fN2.getNHIs(model = 0)
         dAs = fN1.getdAs(model = 0)
         dlogp = np.log10(params[0] -fid)
         for j in range(len(NHI1)):
             NHI = max(NHI1[j],NHI2[j])
             NHIs.append(NHI)
         NHIs.append(10**21.5)
         NHIs.append(10**21.6)
         NHIs.append(10**21.7)
         NHIs = sorted(NHIs)
         for j in range(len(NHIs)):
             dlogfdlogps.append(fN2.f(NHIs[j],NHI1,dAs)-fN1.f(NHIs[j],NHI1,dAs))
             #dlogfdlogps.append( np.log10((fN2.f(NHIs[j],NHI1,dAs)/(fN1.f(NHIs[j],NHI2,dAs)/dlogp))
             #dlogfdlogps.append(np.log10((fN2.f(NHIs[j],NHI1,dAs)/(fN1.f(NHIs[j],NHI2,dAs))/dlogp))
         
         #return dlogfdlogps
         
      
         

if __name__ == '__main__':
    exper = 'rg01'
    exper2 = 'za01'
    param = 'mu'
    time = 0
    time2 = 201
    fig  = plt.figure()
    ax = fig.add_subplot(111)
    der = dlogfNHI(exper,exper2,param,time, time2)
    x,y = der.dlogf()
    ax.scatter(x,y,lw= 0)
    plt.title(r'Varied $\mu$ at $z=2.5,1,0$')
    plt.ylabel(r'$d\log_{10}f(N_{\mathrm{H}\mathrm{I}})/d\log_{10}\mu$')
    plt.xlabel(r'N$_{H\mathrm{I}}$')
    plt.savefig(exper2 + "vary" + param)
    
