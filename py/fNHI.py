#Rohan Choudhury
#fNHI.py
#this module will create a histogram using the N_H! column density
#for every cell in the simulated model class by converting data
#from fPerp using the integral described in Erkal et al(2013)

import fPerpDistribution as fP
import numpy as np
import matplotlib.pyplot as plt
from readoutput import *

class fNHI:
    def __init__(self,expName,time): #takes in experiment name and time instance
        self.name = expName
        self.ti = time
    def createHistogram(self,modelNum = 0):# converts data from fPerp
        exp = fP.FPerp(self.name,self.ti)
        binCenters,binVals = exp.getVals()
        if(modelNum != 0):
            binCenters,binVals = exp.getVals(model = modelNum)
        edge = exp.getEdge()
       
        binVals2 = [0] * len(binVals)
        for i in range(len(binCenters)):#does the erkal integral
            val = 0
            for count in range(i+1):
                val = val + edge * pow(pow(10.0,binCenters[count]),2) * binVals[count]
            val = val * pow(pow(10.0,binCenters[i]),-3)
            
            binVals2[i] = val
     #   print binCenters
     
        return binCenters,binVals2
if __name__ == '__main__':
    name = 'rg01'
    exp = fNHI(name,0)
    x,y =  exp.createHistogram()
    fig = plt.figure()
    ax = fig.add_subplot(111)
   # ax.set_ylim(-22,-4)
    print np.log10(y)
    plt.plot(x,np.log10(y))
    plt.savefig("fNHI.png")

