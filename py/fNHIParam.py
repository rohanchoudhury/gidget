#Rohan Choudhury
#fNHIParam.py
# this module is meant to create a plot 
#in one time instance for multiple vals of a parameter
# given a certain experiment.

from readoutput import *
import numpy as np
import matplotlib.pyplot as plt
from fNHI import *

class fNHIParam:
    def __init__(self,expname,param,time):
        self.name = expname
        self.param = param
        self.ti = time
    def plot(self):
        exp = Experiment(self.name)
        exp.read()
        
        params,_,_,_ = exp.constructQuantity(self.param)
        graph = fNHI(self.name,self.ti) 
        x,y =  graph.createHistogram()
<<<<<<< Updated upstream
        gr = fNHI("aa03",self.ti)
=======
        plt.plot(x,np.log10(y),color = 'blue')
        gr = fNHI("aa03",self.ti,)
>>>>>>> Stashed changes
        x2,y2 = gr.createHistogram()
        NHI,_,_,_ = exp.constructQuantity('NHI',timeIndex = self.ti)
        plt.plot(x2,np.log10(y2),color = 'red')

        
        if(self.ti == 0):
            plt.title(r'Varied $t_{SC}$ at $z=2.5$')
        elif(self.ti == 51):
            plt.title(r'Varied $t_{SC}$ at $z=1$')
        elif(self.ti == 201):
            plt.title(r'Varied $t_{SC}$ at $z=0$')

        plt.ylabel(r'$f(N_{H\mathrm{I}})$')
        plt.xlabel(r'N$_{H\mathrm{I}}$')
        plt.savefig(self.name + "varyfNHI" + self.param + str(self.ti) +  ".png")

    def dlnfNHI(self):
        exp = Experiment(self.name)#reads in experiment with variation
        exp.read()
        exp2 = Experiment('rg01')#reads in exp with fiducial vals
        exp2.read()
        
        NHI,_,_,_ = exp.constructQuantity('NHI',timeIndex = self.ti)
        params,_,_,_ = exp.constructQuantity(self.param)
        fidVal,_,_,_ = exp2.constructQuantity(self.param)
        graph = fNHI(self.name,self.ti)
        orig = fNHI('rg01',self.ti)
        origCenters,origfNHI = np.array(orig.createHistogram())

        vals = np.array([0.0] * len(origCenters))
        vals2 = np.array([0.0]*len(origCenters))
        fig = plt.figure()
        ax = fig.add_subplot(111)
        n = len(params)
        print fidVal[0]/params[0]
        for i in range(n):
            centers,fN = graph.createHistogram()
            for j in range(len(fN)):
                print str(origfNHI[j]) + " " + str(fN[j])
                vals[j] = np.log10(origfNHI[j]/fN[j])/np.log10(fidVal[0]/params[i])
           # print vals
            #  ax.plot(centers,vals2,color = (0,0,(255- i * (20/float(n)))/255)) 
            ax.plot(centers,vals,color = (0,0,(255- i * (20/float(n)))/255))
          
        ax.set_xlim(19,24)
        if(self.ti == 0):
            plt.title(r'Varied $t_{SC}$ at $z=2.5$')
        elif(self.ti == 51):
            plt.title(r'Varied $t_{SC}$ at $z=1$')
        elif(self.ti == 201):
            plt.title(r'Varied $t_{SC}$ at $z=0$')

        plt.ylabel(r'$d\log_{10}f(N_{H\mathrm{I}})/dt_{SC}}$')
        plt.xlabel(r'N$_{H\mathrm{I}}$')
        plt.savefig(self.name + "vary" + self.param + str(self.ti) +  ".png")
 
if __name__ == '__main__':
<<<<<<< Updated upstream
    exp = fNHIParam('te03','tDepH2SC',201)
=======
    exp = fNHIParam('te03','tDepH2SC',0)
>>>>>>> Stashed changes
    exp.plot()
   
