#Rohan Choudhury
#fmultiple.py
# Will go through multiple galaxies and weight them
# and add the quantities together.

import mainseq as wgt
import matplotlib.pyplot as plt
from readoutput import *
from fNHI import *
from fPerpDistribution import *
from math import log10

class fmultiple:
    def __init__(self,expName,ti):
        self.name = expName
        global exp
        exp = Experiment(self.name)
        exp.read()
        global time
        time = ti
        global fN
        fN = fNHI(self.name,time)
    def createDistribution(self,N = 100,lLim = 0, hLim = 0):
        mstars,_,_,_ = exp.constructQuantity('mstar',timeIndex = time)
        draws = wgt.drawFromSchechter(N,-1.3,10.0**10.9,10.0**9)
        centers = []
        vals = []
        models = []
        
        for i in range(len(draws)):
            index,val = Nearest(mstars,draws[i])
            if(lLim == 0 and hLim == 0):
                models.append(index)
            elif(mstars[index] > lLim and mstars[index] < hLim):
                models.append(index)
        for i in range(len(models)):
            plt.vlines(log10(fN.getMinNHI(modelNum = i)),-25,-10,colors = 'gray',linestyles = 'dashed')
            if i == 0:
                _,vals = fN.createHistogram(modelNum = i)
            if i != 0:
                centers,values = fN.createHistogram(modelNum = i)
                vals = fmultiple.add(self,vals,values)
        
        return centers, vals
    def add(self,vals,adder):
        for i in range(len(adder)):
            vals[i] = vals[i] + adder[i]
        return vals
    def plot(self):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.title(r'$f(N_{H\mathrm{I}},z)$ with 300 galaxies')
        ax.set_xlim(16,22)
        plt.ylabel(r'$f(N_{H\mathrm{I}},z)$')
        plt.xlabel(r'$N_{H\mathrm{I}}$')
        labels = []
        centers,vals = fmultiple.createDistribution(self,N=50)
        plt.plot(centers,np.log10(vals),label = 'Full Distribution')
        centers,vals = fmultiple.createDistribution(self,N=50,lLim = 10.0e9,hLim = 10.0e10)
        print vals
       # plt.plot(centers,np.log10(vals),'--',color = 'green',label = '9-10 mass bin')
        centers,vals = fmultiple.createDistribution(self,N=50,lLim = 10.0e8,hLim = 10.0e9)
        plt.plot(centers,np.log10(vals), '--',color = 'red',label = '8-9 mass bin')
        centers,vals = fmultiple.createDistribution(self,N=50,lLim = 10.0e7,hLim = 10.0e8)
        plt.plot(centers,np.log10(vals),'--',color = 'yellow',label = '7-8 mass bin')
        plt.legend(('Full Distribution','9-10 mass bin','8-9 mass bin','7-8 mass bin'))
        plt.savefig(self.name + str(time) + 'fNHI4.png')
        
if __name__ == '__main__':
    f = fmultiple('rh06',201)
    f.plot()   
